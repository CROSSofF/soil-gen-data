"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.normalRandom = void 0;
function normalRandom() {
    var u = 0;
    var v = 0;
    while (u === 0)
        u = Math.random();
    while (v === 0)
        v = Math.random();
    return Math.sqrt(-2.0 * Math.log(u)) * Math.cos(2.0 * Math.PI * v);
}
exports.normalRandom = normalRandom;
