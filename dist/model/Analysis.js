"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AnalysisResult = void 0;
var uuid_1 = require("uuid");
var lerp_1 = require("../util/lerp");
var random_1 = require("../util/random");
var SampleType;
(function (SampleType) {
    SampleType[SampleType["reference"] = 0] = "reference";
    SampleType[SampleType["processed"] = 1] = "processed";
})(SampleType || (SampleType = {}));
var DigestionMethod;
(function (DigestionMethod) {
    DigestionMethod[DigestionMethod["H202<40\u00B0C"] = 0] = "H202<40\u00B0C";
    DigestionMethod[DigestionMethod["Fenton's"] = 1] = "Fenton's";
    DigestionMethod[DigestionMethod["NaOH & KOH"] = 2] = "NaOH & KOH";
    DigestionMethod[DigestionMethod["NaClO@pH9.5"] = 3] = "NaClO@pH9.5";
    DigestionMethod[DigestionMethod["HCl"] = 4] = "HCl";
    DigestionMethod[DigestionMethod["H2SO3"] = 5] = "H2SO3";
    DigestionMethod[DigestionMethod["ashed@60\u00B0C"] = 6] = "ashed@60\u00B0C";
})(DigestionMethod || (DigestionMethod = {}));
var AnalysisResult = /** @class */ (function () {
    function AnalysisResult(traceesId, spectrumId) {
        this._id = uuid_1.v4();
        this.traceesId = traceesId;
        this.spectrumId = spectrumId;
        this.sampleType = this.generateSampleType();
        this.digestion = this.generateDigestion();
        this.ph = this.generatePH();
        this.ec = this.generateEC();
        this.totalC = this.generateTotalC();
        this.totalN = this.generateTotalN();
        this.fizzTest = this.generateFizzTest();
        this.lecoData = this.generateLECOData();
        this.cleanUp();
    }
    AnalysisResult.prototype.cleanUp = function () {
        if (this.ph === undefined)
            delete this.ph;
        if (this.ec === undefined)
            delete this.ec;
        if (this.totalC === undefined)
            delete this.totalC;
        if (this.totalN === undefined)
            delete this.totalN;
        if (this.fizzTest === undefined)
            delete this.fizzTest;
        if (this.lecoData === undefined)
            delete this.lecoData;
        if (this.lecoData) {
            if (this.lecoData.detectedN === undefined)
                delete this.lecoData.detectedN;
        }
    };
    AnalysisResult.prototype.generateSampleType = function () {
        var enumValues = Object.keys(SampleType)
            .map(function (n) { return Number.parseInt(n, 10); })
            .filter(function (n) { return !Number.isNaN(n); });
        var randomIndex = Math.floor(Math.random() * enumValues.length);
        var randomEnumValue = enumValues[randomIndex];
        return randomEnumValue;
    };
    AnalysisResult.prototype.generateDigestion = function () {
        var enumValues = Object.keys(DigestionMethod)
            .map(function (n) { return Number.parseInt(n, 10); })
            .filter(function (n) { return !Number.isNaN(n); });
        var randomIndex = Math.floor(Math.random() * enumValues.length);
        var randomEnumValue = enumValues[randomIndex];
        return randomEnumValue;
    };
    AnalysisResult.prototype.generatePH = function () {
        if (Math.random() <= 0.9) {
            return lerp_1.lerp(random_1.normalRandom(), 4, 10);
        }
        else {
            return undefined;
        }
    };
    AnalysisResult.prototype.generateEC = function () {
        if (Math.random() <= 0.9) {
            return lerp_1.lerp(random_1.normalRandom(), 10E-6, 3000E-6);
        }
        else {
            return undefined;
        }
    };
    AnalysisResult.prototype.generateTotalC = function () {
        if (Math.random() <= 0.9) {
            return lerp_1.lerp(random_1.normalRandom(), 0.01, 3.5);
        }
        else {
            return undefined;
        }
    };
    AnalysisResult.prototype.generateTotalN = function () {
        if (Math.random() <= 0.9) {
            return lerp_1.lerp(random_1.normalRandom(), 0.01, 4.5);
        }
        else {
            return undefined;
        }
    };
    AnalysisResult.prototype.generateFizzTest = function () {
        if (Math.random() <= 0.9) {
            if (Math.random() <= 0.7) {
                return "+";
            }
            else {
                return "-";
            }
        }
        else {
            return undefined;
        }
    };
    AnalysisResult.prototype.generateLECOData = function () {
        if (Math.random() <= 0.6) {
            var weightBefore = Math.random() <= 0.5
                ? 2.5 + 2 * (Math.random() - 0.5) * 0.1
                : 5.0 + 2 * (Math.random() - 0.5) * 0.1;
            var detectedC = lerp_1.lerp(random_1.normalRandom(), 0.025, 0.0875);
            var detectedN = Math.random() <= 0.99
                ? lerp_1.lerp(random_1.normalRandom(), 0.01, 4.5)
                : undefined;
            return {
                machineModel: "TEST-LECO",
                weightBefore: weightBefore,
                detectedC: detectedC,
                detectedN: detectedN,
            };
        }
        else {
            return undefined;
        }
    };
    return AnalysisResult;
}());
exports.AnalysisResult = AnalysisResult;
