import { v4 } from "uuid";

interface WavenumberInfo {
    start: number,
    stop: number,
    step: number
}

export class Spectrum {
    private static machineModel = "TEST-PerkinElmer";

    public _id: Readonly<string>;
    public traceesId: Readonly<string>;
    public machineModel: Readonly<string>;
    public absorbances: Readonly<number[]>;
    public wavenumberInfo: Readonly<WavenumberInfo>;

    constructor(traceesId: string) {
        this._id = v4();
        this.traceesId = traceesId;
        this.machineModel = this.generateMachineModel();
        this.absorbances = this.generateAbsorbances();
        this.wavenumberInfo = this.generateWavenumberInfo();
    }



    private generateMachineModel(): string {
        return Spectrum.machineModel;
    }

    private generateAbsorbances(): number[] {
        const results = [];

        for (let i = 0; i <= (7800-450)/2; i++) {
            results.push(0.8+Math.random()/100);
        }

        return results;
    }

    private generateWavenumberInfo(): WavenumberInfo {
        return {
            start: 7800,
            stop: 450,
            step: 2,
        };
    }
}