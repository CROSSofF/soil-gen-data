"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.lerp = void 0;
function lerp(x, start, end) {
    return start + (x * (end - start));
}
exports.lerp = lerp;
