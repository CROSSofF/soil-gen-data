"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Spectrum = void 0;
var uuid_1 = require("uuid");
var Spectrum = /** @class */ (function () {
    function Spectrum(traceesId) {
        this._id = uuid_1.v4();
        this.traceesId = traceesId;
        this.machineModel = this.generateMachineModel();
        this.absorbances = this.generateAbsorbances();
        this.wavenumberInfo = this.generateWavenumberInfo();
    }
    Spectrum.prototype.generateMachineModel = function () {
        return Spectrum.machineModel;
    };
    Spectrum.prototype.generateAbsorbances = function () {
        var results = [];
        for (var i = 0; i <= (7800 - 450) / 2; i++) {
            results.push(0.8 + Math.random() / 100);
        }
        return results;
    };
    Spectrum.prototype.generateWavenumberInfo = function () {
        return {
            start: 7800,
            stop: 450,
            step: 2,
        };
    };
    Spectrum.machineModel = "TEST-PerkinElmer";
    return Spectrum;
}());
exports.Spectrum = Spectrum;
