import { Spectrum } from "./model/Spectrum";
import { MongoClient } from "mongodb";
import { Sample } from "./model/Sample";
import { AnalysisResult as Analysis } from "./model/Analysis";

const client = new MongoClient("mongodb://dune-user:password@localhost:27017", {
    ignoreUndefined: true,
})

function generateSamples() {
    const result = [];
    
    for (let i = 0; i < 1000; i++) {
        result.push(new Sample());
    }

    return result;
}

function generateSpectrums(samples: Sample[]) {
    const result = [];
    
    for (const sample of samples) {      
        let i = 0;
        let probability = 1;
        while (Math.random() < probability && i <= 4) {
            result.push(new Spectrum(sample.traceesId));
            i++;
            probability *= 0.3
        }
    }

    return result;
}

function generateAnalyses(samples: Sample[], spectrums: Spectrum[]) {
    const result = [];

    for (const sample of samples) {
        const relevantSpectrums = spectrums.filter((spectrum) => spectrum.traceesId === sample.traceesId);

        for (const spectrum of relevantSpectrums) {
            let i = 0;
            let probability = 1;
            while (Math.random() <= probability && i <= 3) {
                result.push(new Analysis(sample.traceesId, spectrum._id));
    
                i++;
                probability *= 0.6;
            }
        }

    }

    return result;
}

async function run() {
    const samples = generateSamples();
    const spectrums = generateSpectrums(samples);
    const analyses = generateAnalyses(samples, spectrums);

    try {
        console.log("Connected successfully to server");

        await client.connect();
        await client.db("dune-test").dropDatabase();

        const db = await client.db("dune-test");
        const samplesCollection = await db.createCollection("samples");
        const spectrumsCollection = await db.createCollection("spectrums");
        const analysesCollection = await db.createCollection("analyses");

        console.log("Inserting samples...")
        await samplesCollection.insertMany(samples);

        console.log("Inserting spectrums...")
        await spectrumsCollection.insertMany(spectrums);

        console.log("Inserting analyses...")
        await analysesCollection.insertMany(analyses);

    } finally {
        await client.close();
    }
}

run().catch(console.dir);