"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var Spectrum_1 = require("./model/Spectrum");
var mongodb_1 = require("mongodb");
var Sample_1 = require("./model/Sample");
var Analysis_1 = require("./model/Analysis");
var client = new mongodb_1.MongoClient("mongodb://dune-user:password@localhost:27017", {
    ignoreUndefined: true,
});
function generateSamples() {
    var result = [];
    for (var i = 0; i < 1000; i++) {
        result.push(new Sample_1.Sample());
    }
    return result;
}
function generateSpectrums(samples) {
    var result = [];
    for (var _i = 0, samples_1 = samples; _i < samples_1.length; _i++) {
        var sample = samples_1[_i];
        var i = 0;
        var probability = 1;
        while (Math.random() < probability && i <= 4) {
            result.push(new Spectrum_1.Spectrum(sample.traceesId));
            i++;
            probability *= 0.3;
        }
    }
    return result;
}
function generateAnalyses(samples, spectrums) {
    var result = [];
    var _loop_1 = function (sample) {
        var relevantSpectrums = spectrums.filter(function (spectrum) { return spectrum.traceesId === sample.traceesId; });
        for (var _i = 0, relevantSpectrums_1 = relevantSpectrums; _i < relevantSpectrums_1.length; _i++) {
            var spectrum = relevantSpectrums_1[_i];
            var i = 0;
            var probability = 1;
            while (Math.random() <= probability && i <= 3) {
                result.push(new Analysis_1.AnalysisResult(sample.traceesId, spectrum._id));
                i++;
                probability *= 0.6;
            }
        }
    };
    for (var _i = 0, samples_2 = samples; _i < samples_2.length; _i++) {
        var sample = samples_2[_i];
        _loop_1(sample);
    }
    return result;
}
function run() {
    return __awaiter(this, void 0, void 0, function () {
        var samples, spectrums, analyses, db, samplesCollection, spectrumsCollection, analysesCollection;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    samples = generateSamples();
                    spectrums = generateSpectrums(samples);
                    analyses = generateAnalyses(samples, spectrums);
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, , 11, 13]);
                    console.log("Connected successfully to server");
                    return [4 /*yield*/, client.connect()];
                case 2:
                    _a.sent();
                    return [4 /*yield*/, client.db("dune-test").dropDatabase()];
                case 3:
                    _a.sent();
                    return [4 /*yield*/, client.db("dune-test")];
                case 4:
                    db = _a.sent();
                    return [4 /*yield*/, db.createCollection("samples")];
                case 5:
                    samplesCollection = _a.sent();
                    return [4 /*yield*/, db.createCollection("spectrums")];
                case 6:
                    spectrumsCollection = _a.sent();
                    return [4 /*yield*/, db.createCollection("analyses")];
                case 7:
                    analysesCollection = _a.sent();
                    console.log("Inserting samples...");
                    return [4 /*yield*/, samplesCollection.insertMany(samples)];
                case 8:
                    _a.sent();
                    console.log("Inserting spectrums...");
                    return [4 /*yield*/, spectrumsCollection.insertMany(spectrums)];
                case 9:
                    _a.sent();
                    console.log("Inserting analyses...");
                    return [4 /*yield*/, analysesCollection.insertMany(analyses)];
                case 10:
                    _a.sent();
                    return [3 /*break*/, 13];
                case 11: return [4 /*yield*/, client.close()];
                case 12:
                    _a.sent();
                    return [7 /*endfinally*/];
                case 13: return [2 /*return*/];
            }
        });
    });
}
run().catch(console.dir);
