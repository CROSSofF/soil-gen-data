import { v4 } from "uuid";
import { lerp } from "../util/lerp";
import { normalRandom } from "../util/random";

enum SampleType {
    "reference",
    "processed"
}

enum DigestionMethod {
    "H202<40°C",
    "Fenton's",
    "NaOH & KOH",
    "NaClO@pH9.5",
    "HCl",
    "H2SO3",
    "ashed@60°C",
}

interface Results {
    ph?: number;
    ec?: number; 
    totalC?: number; 
    totalN?: number; 
    fizzTest?: "+" | "-";
}

interface LECOData {
    machineModel: Readonly<string>;
    weightBefore: Readonly<number>; 
    detectedC: Readonly<number>;
    detectedN?: Readonly<number>;
}

export class AnalysisResult {
    public _id: Readonly<string>;
    public traceesId: Readonly<string>;
    public spectrumId: Readonly<string>;
    public sampleType: Readonly<SampleType>;
    public digestion: Readonly<DigestionMethod>;
    public ph?: Readonly<number>;
    public ec?: Readonly<number>; 
    public totalC?: Readonly<number>; 
    public totalN?: Readonly<number>; 
    public fizzTest?: Readonly<"+" | "-">;
    public lecoData?: LECOData;

    constructor(traceesId: string, spectrumId: string) {
        this._id = v4();
        this.traceesId = traceesId;
        this.spectrumId = spectrumId;
        this.sampleType = this.generateSampleType();
        this.digestion = this.generateDigestion();
        this.ph = this.generatePH();
        this.ec = this.generateEC();
        this.totalC = this.generateTotalC();
        this.totalN = this.generateTotalN();
        this.fizzTest = this.generateFizzTest();
        this.lecoData = this.generateLECOData();

        this.cleanUp();
    }

    private cleanUp() {
        if (this.ph === undefined) delete this.ph;
        if (this.ec === undefined) delete this.ec;
        if (this.totalC === undefined) delete this.totalC;
        if (this.totalN === undefined) delete this.totalN;
        if (this.fizzTest === undefined) delete this.fizzTest;
        if (this.lecoData === undefined) delete this.lecoData;

        if (this.lecoData) {
            if (this.lecoData.detectedN === undefined) delete this.lecoData.detectedN;
        }
    }

    private generateSampleType() {
        const enumValues = Object.keys(SampleType)
              .map(n => Number.parseInt(n, 10))
              .filter(n => !Number.isNaN(n)) as unknown as SampleType[keyof SampleType][]
        const randomIndex = Math.floor(Math.random() * enumValues.length)
        const randomEnumValue = enumValues[randomIndex]
        return randomEnumValue as any as SampleType;
    }

    private generateDigestion() {
        const enumValues = Object.keys(DigestionMethod)
              .map(n => Number.parseInt(n, 10))
              .filter(n => !Number.isNaN(n)) as unknown as DigestionMethod[keyof DigestionMethod][]
        const randomIndex = Math.floor(Math.random() * enumValues.length)
        const randomEnumValue = enumValues[randomIndex]
        return randomEnumValue as any as DigestionMethod;
    }

    private generatePH() {
        if (Math.random() <= 0.9) {
            return lerp(normalRandom(), 4, 10);
        } else {
            return undefined;
        }
    }

    private generateEC() {
        if (Math.random() <= 0.9) {
            return lerp(normalRandom(), 10E-6, 3000E-6);
        } else {
            return undefined;
        }
    }

    private generateTotalC() {
        if (Math.random() <= 0.9) {
            return lerp(normalRandom(), 0.01, 3.5);
        } else {
            return undefined;
        }
    }

    private generateTotalN() {
        if (Math.random() <= 0.9) {
            return lerp(normalRandom(), 0.01, 4.5);
        } else {
            return undefined;
        }
    }

    private generateFizzTest() {
        if (Math.random() <= 0.9) {
            if (Math.random() <= 0.7) {
                return "+";
            } else {
                return "-"
            }
        } else {
            return undefined;
        }
    }

    private generateLECOData() {
        if (Math.random() <= 0.6) {
            const weightBefore = Math.random() <= 0.5 
                    ? 2.5 + 2*(Math.random() - 0.5)*0.1 
                    : 5.0 + 2*(Math.random() - 0.5)*0.1
            const detectedC = lerp(normalRandom(), 0.025, 0.0875);
            const detectedN = Math.random() <= 0.99
                    ? lerp(normalRandom(), 0.01, 4.5)
                    : undefined
            return {
                machineModel: "TEST-LECO",
                weightBefore, 
                detectedC,
                detectedN,
            }
        } else {
            return undefined;
        }
    }
}