"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var fs_1 = __importDefault(require("fs"));
var sampleFile = fs_1.default.readFileSync("D:/git/soil-test-server/res/testSamples.json");
var sampleData = JSON.parse(sampleFile);
var wavenumberFile = fs_1.default.readFileSync("D:/git/soil-test-server/res/wavenumbers.json");
var wavenumberData = JSON.parse(wavenumberFile);
var app = express_1.default();
var port = 9443; // default port to listen
app.get("/data", function (req, res) {
    res.send(sampleData);
});
app.get("/wavenumbers", function (req, res) {
    res.send(wavenumberData);
});
// start the Express server
app.listen(port, function () {
    console.log("server started at http://localhost:" + port);
});
